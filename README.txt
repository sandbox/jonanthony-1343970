1. Download the telesocial SDK from https://github.com/telesocial/Telesocial-SDK-PHP and add it to your sites/all/libraries folder
2. Enable the module
3. Go to admin/config/telesocial and type in your Telesocial API key
4. Set the apropriate permissions 
5. Under user/%/edit you can add your Telesocial ID or register a new one.
6. Enable the Talkspaces block *optional under admin/structure/blocks

That should be it really but this README file is subject to change :)
